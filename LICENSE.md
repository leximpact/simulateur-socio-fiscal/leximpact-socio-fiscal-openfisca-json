# LexImpact Socio-Fiscal OpenFisca JSON

_JSON & YAML data extracted from OpenFisca-France-with-indirect-taxation source code (parameters, variables, etc)_

- [OpenFisca-France](https://github.com/openfisca/openfisca-france) contributors,
- [OpenFisca-France-indirect-taxation](https://github.com/openfisca/openfisca-france-indirect-taxation) contributors,
- [Toufic Batache](mailto:toufic.batache@assemblee-nationale.fr),
- [Sandra Chakroun](mailto:sandra.chakroun@assemblee-nationale.fr),
- [Chloé Lallemand](mailto:chloe.lallemand@assemblee-nationale.fr),
- [Dorine Lambinet](mailto:dorine.lambinet@assemblee-nationale.fr),
- [Emmanuel Raviart](mailto:emmanuel.raviart@assemblee-nationale.fr),
- Ahmed Zemzami

Copyright (C) 2013-2023 OpenFisca-France contributors
Copyright (C) 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/leximpact/leximpact-socio-fiscal-openfisca-json

> LexImpact Socio-Fiscal OpenFisca JSON is free software; you can redistribute
> it and/or modify it under the terms of the GNU Affero General Public
> License as published by the Free Software Foundation, either version 3
> of the License, or (at your option) any later version.
>
> LexImpact Socio-Fiscal OpenFisca JSON is distributed in the hope that it will
> be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
> of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
