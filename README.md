# LexImpact Socio-Fiscal OpenFisca JSON

_JSON & YAML data extracted from OpenFisca-France-with-indirect-taxation source code (parameters, variables, etc)_

These data are extracted from [OpenFisca-France-with-indirect-taxation source code](https://git.leximpact.dev/leximpact/openfisca-france-with-indirect-taxation) using [OpenFisca JSON Model](https://git.leximpact.dev/leximpact/openfisca-json-model) tools.


**Sommaire :**
1. [Mettre en préprod les changements](#mettre-en-préprod-les-changements)
2. [Ajouter / modifier des variables dans le fichier customizations.json](#ajouter--modifier-des-variables-dans-le-fichier-customizationsjson)
3. [Ajouter un cas type](#ajouter-un-cas-type)


## Mettre en Préprod les changements

Pour mettre en préprod les changements apportés dans ce dépôt, et pour permettre de les voir en local sur socio-fiscal-ui, il faut : 
1. Merger la branche qui apporte les changements
2. Cliquer sur "Build" dans le menu à gauche, puis sur "Pipelines"
3. Cliquer sur "Run Pipeline", une page s'ouvre, rester sur la branche Master, lancer le processus.


Une fois toutes les pipelines passées au vert, les changements sont en préprod. ⚠️ Ne pas cliquer sur les boutons "Play" dans le schémas des pipelines, ça peut lancer une mise en prod ... !

## Ajouter / modifier des variables dans le fichier customizations.json


Le fichier `customizations.json` permet d'apporter des modifications aux variables extraites directement d'OpenFisca, qui se trouvent dans le fichier `decomposition_extracted.json`.



### Modifier les labels, ajouter un short_label

```json
"ircantec_employeur": {
    "label": "Cotisation employeur à l'institution de retraite complémentaire des agents non-titulaires de l'État et des collectivités publiques (Ircantec)",
    "short_label": "Cotis. retraite complémentaire non-titulaires (Ircantec)",
}
```

### Ajouter une description

La description est composée : 
- `href` : Lien vers un site externe de référence
- `note` : Texte de la description
- `title` : Titre de la référence
- une date, date à laquelle le lien fourni a été visité. Si pas de lien `title` ni `href`, alors mettre `0001-01-01`

```json
    "description": {
      "2021-07-27": [
        {
          "href": "https://www.urssaf.fr/portail/home/employeur/calculer-les-cotisations/les-taux-de-cotisations/la-cotisation-vieillesse.html",
          "note": "Cette cotisation est une des deux fractions de la cotisation employeur de l'assurance vieillesse. Cette dernière finance une part du régime de retraite de base des salariés du secteur privé et des non-titulaires de la fonction publique. (L'autre part étant financée par les cotisations salariales). \nOn parle de fraction déplafonnée car le montant de cette part de la cotisation est basé sur la rémunération totale.",
          "title": "Urssaf.fr"
        }
      ]
    }
```

### Ajouter une référence législative

La référence est composée : 
- `href` : Lien vers la référence législative
- `title` : Titre de la référence 
- une date : toujours laisser `0001-01-01`

```json
    "reference": {
      "0001-01-01": [
        {
          "href": "https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006073189/LEGISCTA000006141693/#LEGISCTA000006141693",
          "title": "Articles L821-1 à L821-8 du Code de la sécurité sociale"
        }
      ]
    }
```

### Ajouter des variables enfants

Le fichier `customizations.json` permet aussi de changer les enfants d'une variable. Il vaut mieux faire cette opération dans OpenFisca cependant : 

```json
"children": [
      {
        "name": "revenu_disponible"
      },
      {
        "name": "taxes_tous_carburants"
      }
    ],
```

### Ajouter des variables liées

Ces variables apparaitront dans la vue focus dispositif au côté de la variable sélectionnée.

#### Les variables qui composent la variable sélectionnée

Ces variables seront affichées avec le préfixe "dont".
Par exemple, pour l'impôt sur le revenu, la varialbe `contribution_exceptionnelle_hauts_revenus` est une `linked_added_variables` car la CEHR est incluse dans l'IR.

Ce qui donnera sur l'interface  : 
_X € d'impôt sur le revenu, dont X € de CEHR_

```json
    "linked_added_variables": [
      "contribution_exceptionnelle_hauts_revenus",
      "credits_impot"
    ],
```

#### Les variables qui influencent la variable sélectionnée

Par exemple, le nombre de part de quotient familial par rapport à l'impôt sur le revenu.

```json
    "linked_other_variables": [
      "nbptr",
      "rfr",
      "csg_deductible_salaire",
      "csg_deductible_non_salarie",
      "csg_deductible_chomage",
      "csg_deductible_retraite",
      "csg_revenus_capital"
    ],
```

#### Utiliser `"linked_..._variables": true,` et

TODO expliquer à quoi ça sert 
`"linked_other_variables": true,`
`"linked_added_variables": true,`


## Afficher/cacher la variable selon les caractéristiques du cas type

Le tableau options permet d'indiquer quand une variable doit être affichée. 
Par exemple, il permet de cacher la réduction générale de cotisations employeur pour les agents publics, afin de l'afficher uniquement pour les actifs salariés : 

```json
    "options": {
      "activite": "actif",
      "categorie_salarie": ["prive_cadre", "prive_non_cadre"],
      "else": {
        "hidden": true
      }
    }
```

Il est possible d'utiliser n'importe quelle variable de cas type pour paramétrer ce choix : 

```json
    "options": {
      "nombre_litres_essence_sp95": true,
      "nombre_litres_essence_sp95_e10": true,
      "nombre_litres_essence_sp98": true,
      "nombre_litres_gazole_b7": true,
      "nombre_litres_gazole_b10": true,
      "nombre_litres_gpl_carburant": true,
      "else": {
        "hidden": true
      }
    }
```

### Modifier la liste des paramètres

Les formules OpenFisca ne permettent pas toujours d'afficher tous les paramètres utiles, ou bien affichent des paramètres obsolètes.
Quand on ajoute un main_parameters, la liste est figée et dans l'ordre indiqué : 

```json
    "main_parameters": [
      "prestations_sociales.solidarite_insertion.autre_solidarite.aefa"
    ],
```

### Ne pas afficher une variable

Pour ne pas afficher une variable dans la feuille de paie, quelque soit la situation, il faut ajouter le champ `"hidden": true`

```json
    "hidden": true,
```

### Indiquer la validité d'une variable

#### La variable est obsolète

Indiquer que la variable est obsolète avec `"obsolote"` et éventuellement la cacher avec le champ `"hidden"` :

```json
    "hidden": true,
    "obsolete": true
```

#### La variable est obsolète

```json
    "last_value_still_valid_on": "2022-07-11"
``` 

### Ajouter une variable qui ne fait pas partie d'OpenFisca

Il peut parfois être nécessaire de créer une variable dans la feuille de paie, qui n'existe pas dans OpenFisca. Il faut le spécifier dans le fichier json : 

```json
    "virtual": true,
```

## Ajouter un cas type

⚠️ Les caractéristiques d'un cas type doivent UNIQUEMENT être des variables d'entrée et non des formules. Autrement, le modèle OpenFisca aligne tous les cas types sur la formule du cas type concerné. 

> Par exemple : Si on entre le salaire imposable (Formule) dans un cas type au lieu du salaire de base (variable d'entrée). Le salaire de base ne sera plus pris en compte pour les autres cas types.